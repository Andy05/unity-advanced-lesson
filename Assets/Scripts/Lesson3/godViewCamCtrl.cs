using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityStandardAssets.Utility;

public class godViewCamCtrl : MonoBehaviour
{
    public Transform followTarget;
    public float followDistance;
    public float scrollSpeed;
    private Vector3 followDirection;
    // Start is called before the first frame update
    void Start()
    {
        followDirection = transform.position - followTarget.position;
        followDistance = followDirection.magnitude;
        Debug.Log("followDistance " + followDistance);
        followDirection.Normalize();
    }

    public void MoveCam()
    {
        Vector2 vScroll = Input.mouseScrollDelta;
        followDistance -= vScroll.y * scrollSpeed;//滾輪往上滾為放大畫面。
        if (followDistance < 5)//設定放大值範圍。
        {
            followDistance = 5.0f;
        }
        else if (followDistance > 30.0f)//設定縮小值範圍。
        {
            followDistance = 30.0f;
        }

        Vector3 camPos = followTarget.position + followDirection * followDistance;//讓攝影機跟著玩家走。
        transform.position = camPos;
    }

    // Update is called once per frame
    //void LateUpdate()
    // {
    // MoveCam();
    // }
}
