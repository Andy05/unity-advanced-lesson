using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class godViewMouseCtrl : MonoBehaviour
{

    public LayerMask iLayerMask;
    public float moveSpeed = 1.0f;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    private void MoveForward(Vector3 dir, float fMoveAmount)
    {
        // Vector3 vOriDir = dir;
        dir.y = 0.0f;
        transform.forward = dir;
        Vector3 moveTarget = transform.position + dir * fMoveAmount;
        Vector3 moveTargetUp = moveTarget;
        moveTargetUp.y += 1.0f;//我每次往高地走，y值就提高1.0f。
        RaycastHit rh;
        if (Physics.Raycast(moveTargetUp, -Vector3.up, out rh, 5.0f, iLayerMask))
        {
            Vector3 cPos = transform.position;
            if (cPos.y - rh.point.y > 0.5)
            {
            }
            else
            {
                transform.position = rh.point;
            }
            Debug.Log("hit");
        }
        /*if (Physics.Raycast(moveTargetUp, -Vector3.up, out rh, 1.2f, iLayerMask))可以不用以上作法判斷，但距離1.2f必須要算清楚。
        {
            transform.position = rh.point;
            Debug.Log("hit");
        }*/
        else
        {
            Debug.Log("no hit");
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 sPos = Input.mousePosition;
            Debug.Log("mouse pos " + sPos);
            Ray r = cam.ScreenPointToRay(sPos);
            RaycastHit rh;
            if (Physics.Raycast(r, out rh, 1000.0f, iLayerMask))
            {
                Vector3 vDir = rh.point - transform.position;
                float fMoveAFrame = moveSpeed * Time.deltaTime;
                if (vDir.magnitude < fMoveAFrame)
                {
                    transform.position = rh.point;
                }
                else
                {
                    vDir.Normalize();
                    MoveForward(vDir, fMoveAFrame);
                }

            }

        }

        Main2.Instance().camCtrl.MoveCam();

    }
}
