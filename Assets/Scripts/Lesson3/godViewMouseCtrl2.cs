using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class godViewMouseCtrl2 : MonoBehaviour
{

    public LayerMask iLayerMask;
    public float moveSpeed = 1.0f;
    private Camera cam;
    private CharacterController cc;
    private float JumpVel = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        cc = GetComponent<CharacterController>();
    }

    private void MoveForward(Vector3 dir, float fMoveAmount)
    {
      //cc.SimpleMove(dir * moveSpeed); 可以自動做平面移動。
        cc.Move(dir * fMoveAmount);//可以做y軸運動，但需再乘上Time.deltaTime。
        // Vector3 vOriDir = dir;
        /*   dir.y = 0.0f;
           transform.forward = dir;
           Vector3 moveTarget = transform.position + dir * fMoveAmount;
           Vector3 moveTargetUp = moveTarget;
           moveTargetUp.y += 1.0f;
           RaycastHit rh = new RaycastHit();
           if (Physics.Raycast(moveTargetUp, -Vector3.up, out rh, 1.2f, iLayerMask))
           {
              // Vector3 cPos = transform.position;
               //if (cPos.y - rh.point.y > 0.5)
               //{
               //}
               //else
               //{
                   transform.position = rh.point;
               //}
               Debug.Log("hit");
           } else
           {
               Debug.Log("no hit");
           }*/

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 vMoveF = Vector3.zero;//Physics.gravity;
        Vector3 vFoot = transform.position;
        vFoot.y += 0.01f;
        Vector3 vFootEnd = transform.position;
        vFootEnd.y -= 0.4f;
      
     
        if (Input.GetMouseButton(0))
        {
            Vector3 sPos = Input.mousePosition;
            Debug.Log("mouse pos " + sPos);
            Ray r = cam.ScreenPointToRay(sPos);
            RaycastHit rh;
            if (Physics.Raycast(r, out rh, 1000.0f, iLayerMask))
            {
                Vector3 vDir = rh.point - transform.position;
                float fMoveAFrame = moveSpeed * Time.deltaTime;
                if (vDir.magnitude < fMoveAFrame)
                {
                    transform.position = rh.point;
                }
                else
                {
                    vDir.Normalize();
                    vMoveF += vDir* moveSpeed;
 
                   // MoveForward(vDir, fMoveAFrame);
                }
              
            }
        }
       
        

        //MoveForward(vMoveF, fMoveAFrame);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Jump");
            JumpVel = 10.0f;
        } else 
        {
            if (JumpVel < 0 && Physics.Linecast(vFoot, vFootEnd, iLayerMask) == true) { //如果從腳底打出投射線，有打到地板，
                JumpVel = 0; //將跳躍力指派為0，避免JumpVel持續被重力往下減。

            }
            JumpVel = JumpVel + Physics.gravity.y * Time.deltaTime;//等於物理公式V=Vo+at，跳躍力慢慢被重力加速度往下拉。

        }
        Debug.Log(JumpVel);
        vMoveF.y += JumpVel;
        cc.Move(vMoveF*Time.deltaTime);
        Main2.Instance().camCtrl.MoveCam();

    }

    private void OnDrawGizmos()
    {
        Vector3 vFoot = transform.position;
        vFoot.y += 0.01f;
        Vector3 vFootEnd = transform.position;
        vFootEnd.y -= 0.2f;
        Debug.DrawLine(vFoot, vFootEnd);
    }
}
