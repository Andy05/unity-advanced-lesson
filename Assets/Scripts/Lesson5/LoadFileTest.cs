using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;

public class LoadFileTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            //string sPath = Application.dataPath; 專案之Assets路徑。
            string sPath = Application.streamingAssetsPath; //專案之StreamingAssets路徑。
            //string sPath = Application.persistentDataPath; //預設存檔位置，每個平台不一樣，Windows會存在C:\User\<user>\AppData\LocalLow\<company name>，一般遊戲紀錄檔都會存在這裡。
            Debug.Log(sPath);
            //SaveTxtfile(sPath + "/ABC.txt", "1234567 fdjshf"); //存檔並在文件裡寫上1234567 fdjshf這段文字。
            //LoadTxtfile(sPath + "/ABC.txt");

            //SaveByteFile(sPath + "/ABC.data");
            LoadByteFile(sPath + "/ABC.data");
        }
    }

    public void LoadTxtfile(string filename) { 
        if(File.Exists(filename) == false)
        {
            return;
        }

        // method 1
        string strs = File.ReadAllText(filename);//這裡面就包含開檔與關檔動作。
        Debug.Log(strs);

        // method 2
        FileStream fs = File.Open(filename, FileMode.OpenOrCreate);//OpenOrCreate:有檔案就開，沒有就創一個。
        StreamReader sr = new StreamReader(fs);
        string sLine = sr.ReadLine();//ReadLine:一行一行讀，ReadToEnd = ReadAllText:一次全部讀進來。
        Debug.Log(sLine);
        sr.Close();
        fs.Close();
    }

    public void SaveTxtfile(string filename, string sAll)
    {
        // method 1
       // File.WriteAllText(filename, sAll);

        // method 2
        FileStream fs = File.Open(filename, FileMode.OpenOrCreate);
        StreamWriter sw = new StreamWriter(fs);
        sw.Write(sAll);
        sw.Close();
        fs.Close();
    }

    public void SaveByteFile(string filename)
    {
        // method 1
      //  byte[] bytes = new byte[1024];
      //  File.WriteAllBytes(filename, bytes);//讀寫整塊記憶體。

        FileStream fs = File.Open(filename, FileMode.OpenOrCreate);
        BinaryWriter bw = new BinaryWriter(fs);
        int iLevel = 10;//假設第10關。
        float fTotalTime = 60.0f;//假設這關能用60秒。
        int inNPC = 20;//假設這關NPC20個。
        string sLevelName = "Level1";
        bw.Write(iLevel);
        bw.Write(fTotalTime);
        bw.Write(inNPC);
        bw.Write(sLevelName);
        bw.Close();
        fs.Close();
    }

    public void LoadByteFile(string filename)
    {
        // method 1
        //  byte[] bytes = new byte[1024];
        //  File.WriteAllBytes(filename, bytes);

        FileStream fs = File.Open(filename, FileMode.OpenOrCreate);
        BinaryReader br = new BinaryReader(fs);
        int iLevel = 0;
        float fTotalTime = 0;
        int inNPC = 0;
        string sLevelName = "";
        iLevel = br.ReadInt32();
        Debug.Log("iLevel " + iLevel.ToString());
        fTotalTime = br.ReadSingle();
        Debug.Log("fTotalTime " + fTotalTime);
        inNPC = br.ReadInt32();
        Debug.Log("inNPC " + inNPC);
        sLevelName = br.ReadString();
        Debug.Log("sLevelName " + sLevelName);//在讀寫二進位檔時，以上Debug.Log的順序要跟一開始宣告賦值的順序一樣，最後印出的值才會是對的。
        br.Close();
        fs.Close();

  
    }
}
