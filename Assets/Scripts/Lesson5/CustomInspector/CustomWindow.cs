﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CustomWindow : EditorWindow {
    

    [MenuItem("Window/Custom")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<CustomWindow>();
    }

    private string sName;

    public void Awake()
    {
        sName = "def";
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField(sName);
        if(GUILayout.Button("Close"))
        {
            this.Close();
        }
    }

    void OnFocus()
    {
        Debug.Log("On Focus");
    }

    void OnLostFocus()
    {
        Debug.Log("Lost Focus");
    }

    void OnDestroy()
    {
        Debug.Log("OnDestroy");
    }
}
