using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Main4 : MonoBehaviour
{
    private static Main4 _instance = null;
    public static Main4 Instance() { return _instance; }

    private GameObject enemyObject = null;

    private LinkedList<GameObject> aaa;
    private List<GameObjectData> _enemies = new List<GameObjectData>();

    private Texture2D enemyTexture;
    private Material enemyMaterial;
    // public godViewCamCtrl camCtrl;

    private void Awake()
    {
        _instance = this;
        ResourceLoader r = new ResourceLoader();
        r.Init();
        //r.LoadObject("mats/Flash");
        // enemyTexture = r.LoadObject("textures/Lightning") as Texture2D;
        enemyTexture = r.LoadTextureObject("Textures/MetalTrim_Albedo");
        enemyMaterial = r.LoadObject("Materials/MetalTrim") as Material;//讀完轉型成Material。
        r.LoadAllObject("Test");

        r.LoadTextObject("Test/Data");
        /*int aaa = 0;
        int bbb = 0;
        for(int a = 0; a < 99999; a++)
        {
            for (int b = 0; b < 99999; b++)
            {
                bbb++;
            }
            aaa++;
        }
        Debug.Log("Awake" + aaa + bbb);*/
        //  Debug.Log("Finish load data " + enemyObject.name);
    }

    private void Start()
    {
        StartCoroutine(ResourceLoader.Instance().LoadGameObjectAsync("BasicEnemy", FinishAsyncLoadGameObject));
    }

    /*private IEnumerator Start()
    {
        int aaa = 0;
        int bbb = 0;
        for (int a = 0; a < 99999; a++)
        {
            Debug.Log("Start" + aaa);
            for (int b = 0; b < 999; b++)
            {
                bbb++;
                Debug.Log("Start" + bbb);
                yield return 0;
            }
            aaa++;
          
            yield return 0;
        }
        StartCoroutine(ResourceLoader.Instance().LoadGameObjectAsync("game1/BasicEnemy", FinishAsyncLoadGameObject));
        Debug.Log("Start" + aaa + bbb);
    }*/

    void FinishAsyncLoadGameObject(Object o)
    {
        enemyObject = o as GameObject;
        ObjectPool.Instance().InitObjectPool(50, enemyObject);
       // GenerateEnemies(20);
        Debug.Log("FinishAsyncLoadObject " + o.name);
    }

    // Start is called before the first frame update
   // void Start()
    //{
      

        //  if (Input.GetMouseButtonDown(0))
        //{
        //    Debug.Log("down");
     //   Debug.Log("Start 0");
     
      //  Debug.Log("Start 1");

       
        // }
    //}

    // Update is called once per frame
    void Update()
    {
        // aaa = new LinkedList<GameObject>();
       if(Input.GetMouseButtonDown(1))
        {
            GenerateEnemies(10);
        }
      //  {
       //     Debug.Log("down");
        //    StartCoroutine(ResourceLoader.Instance().LoadGameObjectAsync("game1/BasicEnemy"));
       // }
       // Debug.Log("time " + Time.deltaTime);
        
    }

    public void RemoveEnemy(GameObject go)
    {
        ObjectPool pool = ObjectPool.Instance();
        for (int i = 0; i < _enemies.Count; i++)
        {
            Debug.Log("RemoveEnemy " + go.name + ":" + _enemies[i].go.name);
            GameObjectData gData = _enemies[i];
            if (gData.go == go)
            {
                Debug.Log("RemoveEnemyIII  " + i);
                _enemies.RemoveAt(i);
                pool.UnLoadObjectToPool(gData);

            }
        }
    }

    private void GenerateEnemies(int num) {

        if(enemyObject == null)
        {
       //     enemyObject = ResourceLoader.Instance().LoadGameObject("game1/BasicEnemy");
        }
        if(_enemies == null)
        {
            _enemies = new List<GameObjectData>();
        }
        ObjectPool pool = ObjectPool.Instance();
       //_enemies = new GameObject[num]; 
        for (int i = 0; i < num; i++)
        {
            GameObjectData gData = pool.LoadObjectFromPool(false);
            GameObject go = gData.go;
            // GameObject go = GameObject.Instantiate(enemyObject);
            // go.GetComponent<Renderer>().material.mainTexture = enemyTexture;
            go.GetComponent<Renderer>().material = enemyMaterial;//把讀進來的Material(材質球)變成Enemy的Material。
            Vector3 vdir = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            if(vdir.magnitude < 0.001f)
            {
                vdir.x = 1.0f;
            }
            vdir.Normalize();
            go.transform.position = vdir * Random.Range(10.0f, 20.0f);
            go.SetActive(true);
            _enemies.Add(gData);
        }
    }


}
