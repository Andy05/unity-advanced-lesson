using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureImage : MonoBehaviour
{
    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Capture(string name)
    {
        //如果不鎖死1920x1080，要做成動態解析度的話，就需要每次都偵測解析度。
        //RenderTexture rt = new RenderTexture(Screen.width);
        //cam.targetTexture = rt;

        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = cam.targetTexture;

        cam.Render();

        Texture2D Image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height);//這裡寫好尺寸就是要照我們設定的RenderTexture尺寸，為1920*1080。
        Image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        Image.Apply();
        RenderTexture.active = currentRT;

        var Bytes = Image.EncodeToPNG();//把原本Bytes陣列(RGBA格式)轉成PNG格式的Bytes。
        Destroy(Image);

        Debug.Log("finish capture : " + Application.dataPath + "/ScreenCaptures/" + name + ".png");
        File.WriteAllBytes(Application.dataPath + "/ScreenCaptures/" + name + ".png", Bytes);
    }

    public void LateUpdate()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Capture("aaa");
        }
    }
}
