using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarLookTarget : MonoBehaviour
{
    public Transform head;
    public Transform lookpt;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        Vector3 vFor = transform.forward;
        Vector3 vec = lookpt.position - head.position;
        Vector3 hvec = vec;
        hvec.y = 0;
        hvec.Normalize();
        float vAngle = Vector3.Angle(vec, hvec); //垂直轉角。
        float hAngle = Vector3.Angle(hvec, vFor); //水平轉角。
        if (hAngle > 30)//限制水平轉角範圍。
        {
            hAngle = 30;

        }
        float fDot = Vector3.Dot(transform.right, hvec);
        if (fDot < 0)
        {
            hAngle = -hAngle;
        }
        if (vAngle > 10)//限制垂直轉角範圍。
        {
            vAngle = 10;
        }
        if (vec.y > 0)
        {
            vAngle = -vAngle;
        }

        // new vector = quaternion * old vector
        //先做水平旋轉，再做垂直旋轉，最後相乘得到新轉向。
        Quaternion qRot0 = Quaternion.Euler(0, hAngle, 0); //將水平轉角轉成Quaternion後，
        Vector3 vecR0 = qRot0 * vFor; //跟舊向量相乘，得到新水平轉向角度之向量。
        Vector3 vAxis = Vector3.Cross(Vector3.up, vecR0); //用新水平向量跟(0,1,0)垂直向量做外積，得到新的水平轉軸(就像一個傳過太陽穴的轉軸)。
        Quaternion qRot = Quaternion.AngleAxis(vAngle, vAxis);//沿著轉軸做垂直旋轉，並轉換成Quaternion。
        vec = qRot * vecR0;

        head.rotation = qRot * qRot0 * head.rotation; //相乘得到新轉向。

        // head.forward = vec;
        // head.Rotate(0, 0, -90, Space.Self);
        // head.Rotate(90, 0, 0, Space.Self);

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(lookpt.position, head.position);
    }
}
