using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public Image playerHpBar;
    public TMP_InputField playerInputField;

    public List<TMP_Text> displayMessages;
    private List<CMessageData> latestMessages = new List<CMessageData>();

    private Canvas _canvas;
    private RectTransform _rt;
    public GameObject _floationTextPrefab;
    public Slider musicSlider;
    public Slider audioSlider;
    public Camera gameCamera;
    public TMP_Dropdown serverSelections;
    public EnemyBarsManager barManager;
    private void Awake()
    {
        instance = this;
        this._canvas = GetComponent<Canvas>(); 
        this._rt = GetComponent<RectTransform>();   
        barManager.mainGameCamera = gameCamera;
    }
    // Start is called before the first frame update
    void Start()
    {
        this.RefreshSettingUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerInputField.Select();
        }
    }

    public void MinusHpBtnClick()
    {
        PlayerCtrl.instance.Damage(10);
    }

    public void UpdatePlayerUIInfo(CPlayerData data)
    {
        float ratio = data.hp / data.maxHp;
        if (ratio < 0)
        {
            ratio = 0;
        }
        else if (ratio > 1)
        {
            ratio = 1;
        }
        playerHpBar.fillAmount = ratio;
    }

    public void TextInputEndEdit(TMP_InputField tmpInput)
    {
        Debug.Log("Input.GetKey(KeyCode.Return) " + Input.GetKey(KeyCode.Return));
        Debug.Log("TextInputEndEdit : " + tmpInput.text);
    }

    public void DeselectTextInput(TMP_InputField tmpInput)
    {
        Debug.Log("DeselectTextInput : " + tmpInput.text);
    }

    public void SendPlayerMessage(int iPlayer)
    {
        string sMessage = playerInputField.text;
        MessageCtrl.instance.AddMessage(iPlayer, sMessage);
    }

    public void RefreshMessageDialog()
    {
        int inDisplay = displayMessages.Count;
        latestMessages.Clear();
        MessageCtrl.instance.getLatestMessages(latestMessages, inDisplay);
        int iCount = latestMessages.Count;
        for(int i = 0; i < iCount; i++)
        {
            displayMessages[i].text = latestMessages[i].playerId.ToString() + ":" + latestMessages[i].message;
        }
        for (int i = iCount; i < inDisplay; i++)
        {
            displayMessages[i].text = "";
        }
    }

    public void OnToggleValueChange(ToggleGroup tg)
    {
        if(tg.AnyTogglesOn() == false) {
            Debug.Log("OnToggleValueChange in group" + "every toggles are off");
            return;
        }

        Debug.Log("OnToggleValueChange in group" + tg.name);
        foreach (Toggle t in tg.ActiveToggles())
        {
            Debug.Log(t.name + ":" + t.isOn);
        }

    }

    public void SetupServerDisplay(List<CServerData> servers)
    {
        List<TMP_Dropdown.OptionData> pList = new List<TMP_Dropdown.OptionData>();
        serverSelections.options = pList;

        foreach(CServerData data in servers)
        {
            TMP_Dropdown.OptionData optionData = new TMP_Dropdown.OptionData();
            optionData.text = data.serverName;
            pList.Add(optionData);
        }
      
    }

    public void SeverSelected()
    {
        Debug.Log("Select server : " + serverSelections.value + ":" + serverSelections.options[serverSelections.value].text);
    }

    public void RefreshSettingUI()
    {
        MainUI m = MainUI.instance;
        musicSlider.onValueChanged.RemoveListener(delegate { onSlideMusicVChanged(); });//避免一進遊戲就呼叫onSlideMusicVChanged，更新資料前先移除Listener。
        musicSlider.value = m.GetMusicVolume();
        musicSlider.onValueChanged.AddListener(delegate { onSlideMusicVChanged(); });//等更新後才加回Listner，避免二次重複更新問題。

        audioSlider.onValueChanged.RemoveListener(delegate { onSlideAudioVChanged(); });
        audioSlider.value = m.GetAudioVolume();
        audioSlider.onValueChanged.AddListener(delegate { onSlideAudioVChanged(); });

    }

    public void onSlideMusicVChanged()
    {
        Debug.Log("AAAAAAAAAAAAAAAAAAAAAA");
        MainUI.instance.SetMusicVolume(musicSlider.value);
    }

    public void onSlideAudioVChanged()
    {
        MainUI.instance.SetAudioVolume(audioSlider.value);
    }

    public FloatingBar RequestFloatingBar(GameObject target)
    {
        return barManager.SpawnBarToTarget(target);
    }

    public void ReleaseFloatingBar(GameObject target)
    {
        barManager.RemoveBarByTarget(target);
    }


    public void SpawnFloatingText(string str, Vector3 sPos)
    {
        FloatingText ft = Instantiate(_floationTextPrefab).GetComponent<FloatingText>();
        ft.transform.SetParent(this.transform);
        ft.SpwanText(str, 1.0f, sPos, this._canvas, this.gameCamera, this._rt);
    }


    public Canvas getCanvas()
    {
        return _canvas;
    }
}
