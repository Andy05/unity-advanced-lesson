using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class EnemyBarsManager : MonoBehaviour
{
    public class CBarData
    {
        public GameObject sourceTarget;
        public FloatingBar bar;
    }
    public GameObject uiBar;
    public Camera mainGameCamera;
    public Canvas gameCanvas;
    private List<CBarData> barList = new List<CBarData>();
    private RectTransform _rt;
    // Start is called before the first frame update
    void Awake()
    {
        _rt = GetComponent<RectTransform>(); 
        generateDefaultBars(50);
    }

    void generateDefaultBars(int iCount)
    {
        for (int i = 0; i < iCount; i++) { 
            CBarData data = new CBarData();
            data.sourceTarget = null;
            data.bar = Instantiate(uiBar).GetComponent<FloatingBar>();
            data.bar.transform.SetParent(this.transform);
            data.bar.UpdateBarValue(1.0f);
            data.bar.gameObject.SetActive(false);
            data.bar.rootCanvas = gameCanvas;
            data.bar.parentRect = _rt;
            barList.Add(data);
        }
    }

    // Update is called once per frame
    /*void LateUpdate()
    {
        int iCount = barList.Count;
        FloatingBar bar = null;
        GameObject source = null;
        Vector3 vPos = Vector3.zero;
        for (int i = 0; i < iCount; i++)
        {
            bar = barList[i].bar;
            source = barList[i].sourceTarget;
            if(source == null || source.activeSelf == false)
            {
                continue;
            }

            bar.UpdatePosition(mainGameCamera, source.transform.position, 1.1f);
        }
    } //以上可改至Enemy與FloatingBar去更新位置。
    */
    public FloatingBar SpawnBarToTarget(GameObject target)
    {
        int iCount = barList.Count;
        for (int i = 0; i < iCount; i++)
        {
            if (barList[i].sourceTarget == null)
            {
                barList[i].sourceTarget = target;
                barList[i].bar.gameObject.SetActive(true);
                barList[i].bar.UpdateBarValue(1.0f);
                return barList[i].bar;
            }
        }
        CBarData data = new CBarData();
        data.sourceTarget = target;
        data.bar = Instantiate(uiBar).GetComponent<FloatingBar>();
        data.bar.transform.parent = this.transform;
        data.bar.rootCanvas = gameCanvas;
        data.bar.parentRect = _rt;
        data.bar.UpdateBarValue(1.0f);
        barList.Add(data);
        return data.bar;
    }

    public void RemoveBarByTarget(GameObject target)
    {
        int iCount = barList.Count;
        for (int i = 0; i < iCount; i++) {
            if(barList[i].sourceTarget == target)
            {
                barList[i].sourceTarget = null;
                barList[i].bar.gameObject.SetActive(false);
                //barList.RemoveAt(i);
                break;
            }
        }
    }
}
