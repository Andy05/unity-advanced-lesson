using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour
{
    public TMP_Text fText;
    public Canvas rootCanvas;
    public RectTransform parentRect;
    public Camera gameCamera;
    private Vector3 _spawnPoint;
    private float _life;

    private void Awake()
    {
        fText = GetComponent<TMP_Text>();
    }

    private void Start()
    {
        if (rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
        {
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }
    }

    public void UpdatePosition(Camera mainCamera, Vector3 followPos)
    {
        Vector3 sPos = mainCamera.WorldToScreenPoint(followPos);
        if (sPos.z < 0.001f)
        {
            fText.enabled = false;
            return;
        }
        fText.enabled = true;
        if (rootCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            this.transform.position = sPos;
        }
        else if (rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
        {
            Vector3 oPos = Vector3.zero;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(parentRect, sPos, rootCanvas.worldCamera, out oPos);
            this.transform.position = oPos;
        }

    }

    // Update is called once per frame
    void LateUpdate()
    {

        _spawnPoint.y += Time.deltaTime * 1.0f;
        this.UpdatePosition(gameCamera, _spawnPoint);
        _life -= Time.deltaTime;
        if(_life < 0)
        {
            Destroy(gameObject);
        }
    }

    public void SpwanText(string str, float duration, Vector3 pt, Canvas ca, Camera cam, RectTransform pRT)
    {
        Debug.Log("SpwanText " + str);
        _spawnPoint = pt;
        fText.text = str;
        _life = duration;
        rootCanvas = ca;
        gameCamera = cam;
        parentRect = pRT;
    }
}
