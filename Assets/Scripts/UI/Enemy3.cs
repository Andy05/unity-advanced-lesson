using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3 : MonoBehaviour
{
    public CEnemyData data;
    private FloatingBar _bar;
    // Start is called before the first frame update
    void Start()
    {
        _bar = UIManager.instance.RequestFloatingBar(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 startPos = gameObject.transform.position;
            startPos.y += 2.0f;
            UIManager.instance.SpawnFloatingText("-10", startPos);
            data.hp -= 10;
            if (data.hp <= 0)
            {
                data.hp = 0;
                UIManager.instance.ReleaseFloatingBar(gameObject);
                Destroy(gameObject);
            }
            float ratio = data.hp / data.maxHp;
            _bar.UpdateBarValue(ratio);
        }
    }

    private void LateUpdate()
    {
        if (_bar != null)
        {
            _bar.UpdatePosition(UIManager.instance.gameCamera, transform.position, 2.2f);
        }
    }
}
