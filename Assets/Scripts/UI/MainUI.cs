using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MainUI : MonoBehaviour
{
    public static MainUI instance;
    public AudioMixer audioMixer;
    private List<CServerData> serverList = new List<CServerData>();
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CServerData sData = new CServerData();
        sData.serverName = "Server A";
        serverList.Add(sData);
        sData = new CServerData();
        sData.serverName = "Server B";
        serverList.Add(sData);
        sData = new CServerData();
        sData.serverName = "Server C";
        serverList.Add(sData);
        sData = new CServerData();
        sData.serverName = "Server D";
        serverList.Add(sData);

        UIManager.instance.SetupServerDisplay(serverList);
    }

    public void SetMusicVolume(float v)
    {
        float ov = 1.0f;
        audioMixer.GetFloat("musicV", out ov);
        if (Mathf.Abs(ov - v) < 0.001f)//如果下面要更新UI，前面就需要綁這幾段，或著在UIManager綁移除Listener的做法，等要更新後才加回Listner，避免二次更新問題。
        {
            return;
        }
        audioMixer.SetFloat("musicV", v);

        // if want to update ui.
        // ...
    }

    public void SetAudioVolume(float v)
    {
        audioMixer.SetFloat("audioV", v);
    }

    public float GetMusicVolume()
    {
        float ov = 1.0f;
        audioMixer.GetFloat("musicV", out ov);
        return ov;
    }

    public float GetAudioVolume()
    {
        float ov = 1.0f;
        audioMixer.GetFloat("audioV", out ov);
        return ov;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
