using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageCtrl : MonoBehaviour
{
    public static MessageCtrl instance;
    private List<CMessageData> pMessages = new List<CMessageData>();
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    public void AddMessage(int iPlayer, string str)
    {
        CMessageData msg = new CMessageData();
        msg.playerId = iPlayer;
        msg.message = str;
        pMessages.Add(msg);//Add:最新的會加到最後面，舊的都會在最底下。用Stack，舊的會在尾，用Quene，舊的會在頭。
        if(pMessages.Count > 10)
        {
            pMessages.RemoveAt(0);
        }
        UIManager.instance.RefreshMessageDialog();
    }

    public void getLatestMessages(List<CMessageData> outDatas, int iWant)
    {
        int iLastID = pMessages.Count - 1;
        int iWantProcessCount = iWant;
        for(int i = iLastID; i >= 0; i--)
        {
            outDatas.Add(pMessages[i]);
            iWantProcessCount--;
            if(iWantProcessCount <= 0)
            {
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
