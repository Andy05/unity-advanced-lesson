using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dropper : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("On Drop1111 " + gameObject.name + ":" + eventData.pointerDrag.name);
        eventData.pointerDrag.transform.SetParent(transform);
        //  throw new System.NotImplementedException();
    }
}
