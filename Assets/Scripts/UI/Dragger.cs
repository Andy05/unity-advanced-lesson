using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Dragger : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    public Image m;
    private Transform _oriParent;
    private Vector3 _deltaVec;
    public void OnBeginDrag(PointerEventData eventData)
    {
        m.raycastTarget = false;//因滑鼠在拖曳的時候等於還點著自己，會導致無法在放開的時候偵測到欄位，因此在拖曳的時候先關掉滑鼠射線偵測。
        Canvas c = UIManager.instance.getCanvas();
        _oriParent = transform.parent;
        _deltaVec = Input.mousePosition - transform.position;
        transform.SetParent(c.transform);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition - _deltaVec;
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("On Drop " + gameObject.name + ":" + eventData.pointerDrag.name);//eventData會記錄現在拖曳的是誰。
        eventData.pointerDrag.transform.SetParent(transform.parent);//將目的地設成自己的父親。
        Destroy(gameObject);//把原本在這上面的物件刪除。
        //  throw new System.NotImplementedException();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Canvas c = UIManager.instance.getCanvas();
        if (transform.parent == c.transform)//若還沒被換父親物件，
        {
            transform.SetParent(_oriParent);//回到原本的物件。
        } 
        transform.localPosition = Vector3.zero;
        m.raycastTarget = true;//再打開才能再觸發下一次拖動。
    }
}
