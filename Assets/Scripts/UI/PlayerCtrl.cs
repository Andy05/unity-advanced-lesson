using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
    public static PlayerCtrl instance;
    [SerializeField]
    public CPlayerData playerData;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        UIManager.instance.UpdatePlayerUIInfo(playerData);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Damage(float damage)
    {
        playerData.hp -= damage;
        if(playerData.hp < 0)
        {
            playerData.hp = 0;
        }
        UIManager.instance.UpdatePlayerUIInfo(playerData);
    }
}
