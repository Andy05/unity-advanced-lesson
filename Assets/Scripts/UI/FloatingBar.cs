using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Unity.VisualScripting.Member;

public class FloatingBar : MonoBehaviour
{
    public Image barImage;
    public Canvas rootCanvas;
    public RectTransform parentRect;


    private void Start()
    {
        if (rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
        {
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }
    }

    public void UpdateBarValue(float value)
    {
        barImage.fillAmount = value;
    }

    public void UpdatePosition(Camera mainCamera, Vector3 followPos, float height)
    {
        followPos.y += height;
        Vector3 sPos = mainCamera.WorldToScreenPoint(followPos);
        if(sPos.z < 0.001f)
        {
            barImage.enabled = false;
            return;
        }
        barImage.enabled = true;
        if (rootCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            this.transform.position = sPos;
        } else if(rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
        {
            Vector3 oPos = Vector3.zero;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(parentRect, sPos, rootCanvas.worldCamera, out oPos);
            this.transform.position = oPos;
            /*
            Vector3 oPos = Vector3.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, sPos, rootCanvas.worldCamera, out oPos);
            this.transform.localposition = oPos;
            */
        }

    }

}
